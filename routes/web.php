<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('cat/create','CatController@create');

Route::post('cat/create','CatController@store');


Route::get('cat','CatController@index');

Route::get('cat/{id}/edit','CatController@edit');

Route::post('cat/update','CatController@update');

Route::get('cat/{id}/delete','CatController@destroy');

Route::get('catlist','CatController@getCatList');

Route::get('usercat','UserController@getUserCat');

Route::get('user/{id}/userdelete','UserController@destroy');
Route::get('user/create','UserController@create');
Route::post('user/create','UserController@store');
// Route::get('user/{id}/edit','UserController@edit');