@extends('layouts.masterlayout')
    @section('title','List Page')
    @section('content')
        <table class="table table-hover">
        <?php foreach($allusers as $user): ?>
            <tr>
                <td><a href="user/{{$user['id']}}/userdelete/">Delete</a></td> <td><a href="user/{{$user['id']}}/useredit/">Edit</a></td>
                <td> {{ $user->id }} </td>
                <td> {{ $user->name }} </td>
                <td>:</td>
                <td> 
                   {{ $user->cats()->get()->pluck('name')->implode(', ') }}
                </td>
            </tr>
        <?php endforeach; ?>
        </table>   
    @endsection