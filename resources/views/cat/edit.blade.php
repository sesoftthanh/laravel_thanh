@extends('layouts.masterlayout')
    @section('title','Edit page')
    @section('content')
    <div class="col-md-8" style="margin-top:50px;">
        <form class="form-horizontal form-row-seperated" action="{{ URL::action('CatController@update') }}"
              method="Post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ old('id', $getCatById['id'])}}">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control"
                       value="{{ old('name', $getCatById['name'])}}" name="name">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    @endsection