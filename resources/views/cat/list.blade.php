@extends('layouts.masterlayout')
    @section('title','List Page')
    @section('content')
    <div class="col-md-8" style="margin-top:50px;">
        <table class="table table-hover">
         <tr>
             <td>Id</td>
             <td>Name</td>
         </tr>
         <?php foreach($allCats as $cat):  ?>
            <tr>
                <td> <?php echo $cat['id']; ?> </td>
                <td> <?php echo $cat['name']; ?></td>
                <td>
                    <a href='cat/<?php echo $cat['id'];?>/edit'> Edit</a>
                    <a href='cat/<?php echo $cat['id'];?>/delete'> Delete</a>
                </td>
            </tr>
          <?php endforeach; ?>
        </table>
    </div>
    @endsection