@extends('layouts.masterlayout')
    @section('title','Insert Page')
    @section('content')
    <div class="col-md-6" style="margin-top:50px;">
        <form class="form-horizontal form-row-seperated" action="{{ URL::action('CatController@store') }}"
        method="Post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" placeholder="Name" name="name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Boss</label>
                <input type="text" class="form-control" placeholder="Boss" name="user_id">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    @endsection
