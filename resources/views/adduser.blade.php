@extends('layouts.masterlayout')
    @section('title','Insert User Page')
    @section('content')
    <div class="col-md-6" style="margin-top:50px;">
        <form class="form-horizontal form-row-seperated" action="{{ URL::action('UserController@store') }}"
        method="Post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control" placeholder="Name" name="name">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="text" class="form-control" placeholder="Email" name="email">
            </div>
            <div class="form-group">
                <label for="">Password</label>
                <input type="text" class="form-control" placeholder="Password" name="password">
            </div>
            <div class="form-group">
                <label for="">Cat1</label>
                <input type="text" class="form-control" placeholder="" name="cat[]">
            </div>
            <div class="form-group">
                <label for="">Cat2</label>
                <input type="text" class="form-control" placeholder="" name="cat[]">
            </div>
            <div class="form-group">
                <label for="">Cat3</label>
                <input type="text" class="form-control" placeholder="" name="cat[]">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    @endsection
