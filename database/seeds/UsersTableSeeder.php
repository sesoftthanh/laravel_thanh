<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // factory(App\User::class, 50)->create();
    	 factory(App\User::class, 50)->create()->each(function($u) {
		    $u->cats()->saveMany(factory(App\Cat::class, rand(1, 4))->make());
		  });
    }
}
