<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Cat;
use Lang;
use View;
use Route;
use DB;
class CatController extends Controller {

	public function index() {
		$objCat =  new Cat();
		$allCats = $objCat->all()->toArray();
		return view('cat.list')->with('allCats',$allCats);
	}

	public function create() {
		return view('cat.create');
	}

	public function store(Request $request) {
		$allRequest = $request->all();
		$name =$allRequest['name'];
		$user_id = $allRequest['user_id'];

		$dataInsertToDatabase = array(

			'name'=>$name,
			'user_id'=>$user_id,		
		);
		$objCat = new Cat();
		$objCat->insert($dataInsertToDatabase);
	}

	public function edit($id) {
		$objCat = new Cat();
		$getCatById = $objCat->find($id)->toArray();
		return view('cat.edit')->with('getCatById',$getCatById);
	}

	public function update(Request $request) {
		$allRequest = $request->all();
		$name = $allRequest['name'];
		$idCat = $allRequest['id'];
		$objCat = new Cat();
		$getCatById = $objCat->find($idCat);
		$getCatById->name = $name;
		$getCatById->save();
		return redirect()->action('CatController@index');
	}


	public function destroy($id)
    {
        Cat::find($id)->delete();
        return redirect()->action('CatController@index');
    }
    public function getCatList(){
  //   	$objCat =  new Cat();
		// $allCats = $objCat->all()->toArray();
		// return view('cat.list')->with('allCats',$allCats);
		// $catlist = DB::table('cats')->get();
		// print_r($catlist);
		// return view('cat.list')->with('allCats',$catlist);
		$allCats = Cat::all()->toArray();
		return view('cat.list')->with('allCats', $allCats);
    }
}