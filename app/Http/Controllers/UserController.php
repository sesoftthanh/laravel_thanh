<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Cat;
use Lang;
use View;
use Route;
use DB;

class UserController extends Controller
{
    public function getUserCat(){
        $obj = new User();
        $allUsers = $obj->get();
        return view('usercat')->with('allusers',$allUsers);
    }
    public function destroy($id){
        User::where('id',$id)->first()->delete();
        // User::find($id)->delete();
        return redirect()->action('UserController@getUserCat');
    }
    public function create(){
        return view('adduser');
    }
    public function store(Request $request) {
        $allRequest = $request->all();
        $name = $allRequest['name'];
        $email = $allRequest['email'];
        $password = $allRequest['password'];
        $objUser = User::firstOrNew([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
        ]);
        
        $objUser->save();
        ////add cat
        $cats = $allRequest['cat'];
        //dd($cats);
        $objUser->cats()->sync($cats);

    }
}
