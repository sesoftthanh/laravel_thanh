<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    protected $table = 'cats'; //This is table's name in database
    //public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User')->withTimestamps();
    }
}
